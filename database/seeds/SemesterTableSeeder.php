<?php

use Illuminate\Database\Seeder;

class SemesterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i = 1; $i <= 8; $i++){
            \App\Semester::create([
                'number' => $i,
            ]);
        }
    }
}
