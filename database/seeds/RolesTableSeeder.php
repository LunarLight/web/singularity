<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Roles
        $roles = [
            'super_admin' => [
                'display_name' => 'Супер-админ',
                'description' => 'Супер-админ',
            ],
            'admin' => [
                'display_name' => 'Администратор',
                'description' => 'Главный администратор',
            ],
            'student' => [
                'display_name' => 'Студент',
                'description' => 'Учащийся института',
            ],
            'teacher' => [
                'display_name' => 'Преподаватель',
                'description' => 'Преподаватель в институте',
            ]
        ];

        foreach ($roles as $key => $role) {
            \App\Role::create([
                'name' => $key,
                'display_name' => $role['display_name'],
                'description' => $role['description'],
            ]);
        }

        \App\User::create([
           'login' => 'admin',
           'password' => bcrypt('qwerty'),
           'role_id' => 1,
        ]);
    }
}
