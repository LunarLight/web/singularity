<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::middleware('auth')->group(function () {
    $this->any('/', function () {
        return view('index');
    })->name('index');


    Route::prefix('users')->name('users.')->group(function () {
        $this->any('/create', 'UserController@create')->name('create');
    });

    Route::prefix('subjects')->name('subjects.')->group(function () {
        $this->get('/', 'SubjectController@view')->name('index');
        $this->any('/create', 'SubjectController@create')->name('create');
        $this->any('/{subject}', 'SubjectController@update')->name('update');
        $this->any('/delete/{subject}', 'SubjectController@delete')->name('delete');
    });

    Route::prefix('faculties')->name('faculties.')->group(function () {
        $this->get('/', 'GroupController@view')->name('index');
        $this->get('/{id}', 'GroupController@directions')->name('direction');
        $this->get('/direction/{id}', 'GroupController@groups')->name('group');
        $this->get('/direction/group/{id}', 'GroupController@students')->name('student');
    });

    Route::prefix('plans')->name('plans.')->group(function () {
        $this->get('/', 'PlanController@semesters')->name('semesters');
        $this->get('/{semester}', 'PlanController@semester')->name('semester');
        $this->any('/{semester}/{group}', 'PlanController@group')->name('group');
    });
    Route::prefix('schedule')->name('schedule.')->group(function () {
        $this->get('/', 'LessonController@index')->name('index');
        $this->any('/{group}', 'LessonController@group')->name('group');
        $this->any('/prevWeek/{group}', 'LessonController@prevWeek')->name('prevWeek');
        $this->any('/add/{group}/{pair}/{day}', 'LessonController@add')->name('add');
        $this->any('/update/{lesson}/{day}', 'LessonController@update')->name('update');
    });

    Route::prefix('knowledge')->name('knowledge.')->group(function () {
        $this->get('/', 'KnowledgeController@index')->name('index');
        $this->post('/add', 'KnowledgeController@add')->name('add');
    });

});
