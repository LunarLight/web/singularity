<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(function () {

    Route::prefix('auth')->group(function () {
        $this->post('login', "AuthController@login");
        $this->get('refresh', 'AuthController@refresh');

        $this->middleware('auth:api')->group(function () {
            $this->get('logout', 'AuthController@logout');
            $this->get('user', 'AuthController@user');
        });
    });



    Route::middleware('auth:api')->group(function () {
        $this->get('/students', 'IndexController@students');
        $this->get('/studentsLessons/{lesson}', 'IndexController@studentsByLesson');
        $this->get('/schedule', 'IndexController@schedule');
        $this->post('/attendances', 'IndexController@attendances');
        $this->get('/attendances/{date}', 'IndexController@attendances');
    });
});
