@extends ('layouts.app')

@section ('content')
    <div class="container">
        <h2>Создание пользователя</h2>
        <form method="POST">
            @csrf

            <div class="form-group">
                <label for="login">Логин</label>
                <input type="text" name="login" class="form-control" id="login" placeholder="Введите логин"
                       value="{{ old('login') }}" required>
            </div>
            <div class="form-group">
                <label for="password">Пароль</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Введите пароль"
                       value="{{ old('password') }}" required>
            </div>
            <div class="form-group">
                <label for="role">Роли</label>
                <select class="custom-select" id="role" name="role">
                    @foreach($roles as $role)
                        <option value="{{ $role->id }}"
                                data-name="{{ $role->name }}">{{ $role->display_name }}</option>
                    @endforeach
                </select>
            </div>
            <div id="student">
                <h3>
                    Данные студента
                </h3>
                <div class="form-group">
                    <label for="first_name">Имя</label>
                    <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Введите имя">
                </div>
                <div class="form-group">
                    <label for="second_name">Фамилия</label>
                    <input type="text" name="second_name" class="form-control" id="second_name"
                           placeholder="Введите фамилию">
                </div>
                <div class="form-group">
                    <label for="middle_name">Отчество (если есть)</label>
                    <input type="text" name="middle_name" class="form-control" id="middle_name"
                           placeholder="Введите отчество (если есть)">
                </div>
                <div class="form-group">
                    <label for="cipher">Шифр</label>
                    <input type="text" name="cipher" class="form-control" id="cipher"
                           placeholder="Введите шифр">
                </div>
                <div class="form-group">
                    <label for="faculty">Факультет</label>
                    <input type="text" name="faculty" class="form-control" id="faculty"
                           placeholder="Введите название факультета">
                </div>
                <div class="form-group">
                    <label for="direction">Направление</label>
                    <input type="text" name="direction" class="form-control" id="direction"
                           placeholder="Введите название направления">
                </div>
                <div class="form-group">
                    <label for="group">Учебная группа</label>
                    <input type="text" name="group" class="form-control" id="group"
                           placeholder="Введите название группы">
                </div>
            </div>
            <div id="teacher">
                <h3>
                    Данные преподавателя
                </h3>
                <div class="form-group_teacher">
                    <label for="first_name">Имя</label>
                    <input type="text" name="first_name" class="form-control" id="first_name"
                           placeholder="Введите имя">
                </div>
                <div class="form-group">
                    <label for="second_name">Фамилия</label>
                    <input type="text" name="second_name" class="form-control" id="second_name"
                           placeholder="Введите фамилию">
                </div>
                <div class="form-group">
                    <label for="middle_name">Отчество (если есть)</label>
                    <input type="text" name="middle_name" class="form-control" id="middle_name"
                           placeholder="Введите отчество (если есть)">
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Создать</button>
        </form>
    </div>
@endsection

@section('scripts')
    <script>

        function close(elem) {
            $(elem).hide();
            $(elem).find('input').attr('disabled', 'true');
        };

        function showElem(elem) {
            $(elem).slideDown(300);
            $(elem).find('input').removeAttr('disabled');
        };

        function checkGroup() {
            $select = $('#student').find('select#group');
            $input = $('#student').find('input#group')
            console.log($select, $input);
            if ($select.length > 0) {
                $input.attr('disabled', 'true');
            }
        };


        $('document').ready(function () {

            close($('#teacher'));
            close($('#student'));

            $('#role').change(function () {
                if ($('#role').val() == 3) {
                    close($('#teacher'));
                    showElem($('#student'));
                    checkGroup();
                } else if ($('#role').val() == 4) {
                    showElem($('#teacher'));
                    close($('#student'));
                } else {
                    close($('#teacher'));
                    close($('#student'));
                }
            });


        })

    </script>

@endsection