@extends ('layouts.app')

@section ('content')

    <div class="container">
        <h2>Список направлений</h2>
        <div class="row">
            @foreach($directions as $direction)
                <div class="col-3" class="border">
                    <a href="{{ route('faculties.group', ['id' => $direction->id]) }}">Направление: {{ $direction->name }}</a>
                </div>
            @endforeach
        </div>
    </div>

@endsection
