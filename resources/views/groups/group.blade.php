@extends ('layouts.app')

@section ('content')

    <div class="container">
        <h2>Список групп</h2>
        <div class="row">
            @foreach($groups as $group)
                <div class="col-3" class="border">
                    <a href="{{ route('faculties.student', ['id' => $group->id]) }}">Группа: {{ $group->name }}</a>
                </div>
            @endforeach
        </div>
    </div>

@endsection
