@extends ('layouts.app')

@section ('content')

    <div class="container">
        <h2>Список факультетов</h2>
        <div class="row">
            @foreach($faculties as $faculty)
                <div class="col-3" class="border">
                    <a href="{{ route('faculties.direction', ['id' => $faculty->id]) }}">Факультет: {{ $faculty->name }}</a>
                </div>
            @endforeach
        </div>
    </div>

@endsection
