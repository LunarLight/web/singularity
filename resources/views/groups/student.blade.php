@extends ('layouts.app')

@section('content')

    <div class="container">
        <h2>
            {{ $group->name }}
        </h2>
        <h3>Список сутдентов:</h3>
            <table class="table">
                <thead>
                <tr>
                    <th>Фамилия</th>
                    <th>Имя</th>
                    <th>Отчество(если есть)</th>
                    <th>Шифр</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($students as $student)
                    <tr>
                    <td>{{ $student->second_name }}</td>
                    <td>{{ $student->first_name }}</td>
                    <td>{{ $student->middle_name }}</td>
                    <td>{{ $student->cipher }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        <p>

            </p>
    </div>

@endsection