@extends('layouts.app')

@section('content')

    <div class="container">
        <h2>Добавление предмета</h2>
        <form method="POST">
            @csrf

            <div class="form-group">
                <label for="name">Название предмета</label>
                <input type="text" name="name" class="form-control" id="name" placeholder="Введите название предмета"
                       value="{{ old('name') }}" required>
            </div>

            <button type="submit" class="btn btn-primary">Добавить</button>
        </form>
    </div>

@endsection