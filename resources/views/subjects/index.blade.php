@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="d-flex flex-row justify-content-between w-100 align-items-center">
            <h2>Список предметов</h2>
            <a href="{{ route('subjects.create') }}" class="btn btn-sm btn-primary">Добавить предмет</a>
        </div>
        <div class="row">
            @foreach($subjects as $subject)
                <div class="col-4">
                    <a href="{{ route('subjects.update', ['$subject' => $subject]) }}">{{ $subject->name }}</a>
                </div>
            @endforeach
        </div>
    </div>

@endsection