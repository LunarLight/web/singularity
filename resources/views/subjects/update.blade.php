@extends('layouts.app')

@section('content')



    <div class="container">
        <h2>{{ $subject->name }}</h2>
        <form method="POST">
            @csrf

            <div class="form-group">
                <label for="name">Название предмета</label>
                <input type="text" name="name" class="form-control" id="name" placeholder="Введите название предмета"
                       value="{{ $subject->name }}" required>
            </div>
            <h4>Список преподавалетелей</h4>
            <div class="row mb-5">
                @foreach($teachers as $teacher)
                    <div class="col-4">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="{{ $teacher->id }}"
                                   id="teacher_{{ $teacher->id }}" name="teachers[]" {{ $subject->teachers->where('id', $teacher->id)->first() ? 'checked' : '' }}>
                            <label class="form-check-label" for="teacher_{{ $teacher->id }}">
                                {{ $teacher->second_name }}
                                {{ $teacher->middle_name }}
                                {{ $teacher->first_name }}
                            </label>
                        </div>
                    </div>
                @endforeach
            </div><h4>Список направлений</h4>
            <div class="row mb-5">
                @foreach($directions as $direction)
                    <div class="col-4">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="{{ $direction->id }}"
                                   id="direction_{{ $direction->id }}" name="directions[]" {{ $subject->directions->where('id', $direction->id)->first() ? 'checked' : '' }}>
                            <label class="form-check-label" for="direction_{{ $direction->id }}">
                                {{ $direction->name }}
                            </label>
                        </div>
                    </div>
                @endforeach
            </div>


            <button type="submit" class="btn btn-primary">Сохранить</button>
            <a href="{{ route('subjects.delete', ['subject' => $subject]) }}" class="btn btn-danger">Удалить</a>
        </form>
    </div>

@endsection