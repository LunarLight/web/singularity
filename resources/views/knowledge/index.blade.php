@extends('layouts.app')

@section('content')

    <div class="modal fade" id="create_modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Добавить материал</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method='post' action="{{ route('knowledge.add') }}" enctype="multipart/form-data"
                          id="add_material">
                        @csrf
                        <input type="text" id="semester" name="semester_id" hidden>
                        <input type="text" id="subject" name="subject_id" hidden>
                        <input type="text" id="teacher" name="teacher_id" value="{{ $id }}" hidden>
                        <div class="form-group">
                            <label for="file_name">Название файла</label>
                            <input type="text" class="form-control" id="file_name" name="file_name"
                                   placeholder="Введите название файла..." required>
                        </div>
                        <div class="form-group">
                            <label for="file">Выберите файл</label>
                            <input type="file" class="form-control-file" id="file" name="file" required>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                    <button type="submit" class="btn btn-primary" form="add_material">Добавить</button>
                </div>
            </div>
        </div>

    </div>

    <!-- Modal -->
    <div class="container">
        <div class="row">
            <div class="col-3 border-right">
                <div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                    @foreach ($subjects as $key => $subject)
                        <a class="nav-link {{ $key == 0 ? 'active':'' }}" data-toggle="pill"
                           href="#subject-{{$subject->id}}" role="tab"
                           aria-selected="true">{{ $subject->name }}</a>
                    @endforeach
                </div>
            </div>
            <div class="col-9">
                <div class="tab-content">
                    @foreach ($subjects as $key => $subject)
                        <div class="tab-pane h-100 fade {{ $key == 0 ? 'active show':'' }}"
                             id="subject-{{ $subject->id }}"
                             role="tabpanel">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    @foreach($semesters as $semester)
                                        <a class="nav-item nav-link semester {{ $semester->number == '1' ? 'active': '' }}"
                                           data-toggle="tab"
                                           href="#semester-{{$semester->id}}" role="tab">{{ $semester->number }}
                                            семестр</a>
                                    @endforeach
                                </div>
                            </nav>
                            <div class="tab-content">
                                @foreach($semesters as $semester)
                                    <div class="tab-pane fade {{ $semester->number == '1' ? 'active show': '' }}"
                                         id="semester-{{$semester->id}}" role="tabpanel"
                                         aria-labelledby="nav-profile-tab">
                                        <div class="w-100 d-flex mt-2 flex-row justify-content-end mb-2">
                                            <button class="btn btn-sm btn-success create_btn"
                                                    data-semester-id="{{ $semester->id }}"
                                                    data-subject-id="{{ $subject->id }}" id="create_material"
                                                    data-toggle="modal"
                                                    data-target="#create_modal">Добавить материал
                                            </button>
                                        </div>
                                        @if (\App\Knowledge::where('semester_id', $semester->id)->get()->first())
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Название</th>
                                                    <th>Ссылка</th>
                                                    <th>Действие</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($knowledge as $elem)
                                                    @if($elem->semester_id == $semester->id)
                                                        <tr>
                                                            <td>{{ $elem->name }}</td>
                                                            <td><a target="_blank"
                                                                   href="{{ \Storage::url($elem->path) }}">Скачать
                                                                    файл</a></td>
                                                            <td>
                                                                <button class="btn btn-sm btn-danger">Удалить</button>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <div>Нет загруженых файлов</div>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.create_btn').click(function () {
                $semester = $(this).data('semesterId');
                $subject = $(this).data('subjectId')
                $('#subject').val($subject);
                $('#semester').val($semester);
            })
        })
    </script>
@endsection