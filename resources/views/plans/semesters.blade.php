@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Список семестров</h2>

        <div class="row">
            @foreach($semesters as $semester)
               <div class="col-3">
                   <a class="btn btn-sm btn-secondary px-4 mb-2" href="{{ route('plans.semester', ['id' => $semester->id]) }}">{{ $semester->number }}</a>
               </div>
            @endforeach
        </div>

    </div>
@endsection
