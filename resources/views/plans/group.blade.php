@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Группа {{ $group->name }} семестр {{ $semester->number }}</h2>
        <form method="POST">
            @csrf

            <div class="row">
                @php
                    $checked = $group->semesters->where('pivot.semester_id', $semester->id)->first();

                    $checked = $checked ? $checked->subjects : []
                @endphp

                @foreach($subjects as $subject)
                    <div class="col-4">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="{{ $subject->id }}"
                                   id="subject_{{ $subject->id }}"
                                   name="subjects[]"
                                    {{ count($checked) > 0 && $checked->where('pivot.subject_id', $subject->id)->first() ? 'checked' : '' }}>
                            <label class="form-check-label" for="subject_{{ $subject->id }}">
                                {{ $subject->name }}
                            </label>
                        </div>
                    </div>
                @endforeach
            </div>
            <button type="submit" class="btn btn-primary mt-2">Сохранить</button>
        </form>
    </div>

@endsection