@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="d-flex flex-row justify-content-between">
            <h2>Группа {{ $group->name  }}</h2>
            <div>
                <span id="prev">Предыдушая неделя</span>
                <span id="next">Следующая неделя</span>
            </div>
        </div>
        <div class="row">
            <div class="col-1">
                <h5>Пара</h5>
                <div class="d-flex flex-column">
                    @foreach ($pairs as $pair)
                        <div class="pair-number">
                            <h5 class>{{ $pair['number'] }} пара</h5>
                            <p class>{{ $pair['pair_time'] }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
            @foreach ($week as $day)
                <div class="col p-1">
                    <h5>{{ $day->format('d.m.Y') }}</h5>
                    <div class="d-flex flex-column">
                        @foreach ($pairs as $pair)
                            @php
                                $dates = \App\Date::where('date', $day->format('Y-m-d'))->get();
                                $lesson = null;
                                foreach ($dates as $date) {
                                    if ($date->lesson->pair_number == $pair['number'] && $date->lesson->group_id == $group->id) {
                                        $lesson = $date->lesson;
                                    }
                                }
                            @endphp
                            @if ($lesson)
                                @php
                                    $subject = $lesson->subject;
                                    $teacher = $lesson->teacher;
                                    $type = $lesson->type == 'practice' ? 'Практика' : $lesson->type == 'lesson' ? 'Лекция' : 'Лаб. работа';
                                @endphp
                                @if(auth()->user()->role_id == '3' || auth()->user()->role_id == '4')
                                    <div class="pair p-2">
                                        <div class="d-flex flex-column h-100">
                                            <span class="d-block text-danger">{{ $subject->name }}</span>
                                            <span class="d-block">{{ $teacher->second_name }}
                                                {{ mb_substr($teacher->first_name, 0, 1) . '.' }}
                                                {{ mb_substr($teacher->middle_name, 0, 1) . '.' }}</span>
                                            <div class="w-100 d-flex flex-row justify-content-between mt-auto">
                                                <span class="d-inline-block text-secondary">{{ $type }}</span>
                                                <span class="d-inline-block text-primary">{{ $lesson->room }}</span>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="pair upd p-2">
                                        <a href="{{ route('schedule.update', ['lesson' => $lesson, 'day' => $day]) }}"
                                           class="mx-auto">
                                            <div class="d-flex flex-column h-100">
                                                <span class="d-block text-danger">{{ $subject->name }}</span>
                                                <span class="d-block">{{ $teacher->second_name }}
                                                    {{ mb_substr($teacher->first_name, 0, 1) . '.' }}
                                                    {{ mb_substr($teacher->middle_name, 0, 1) . '.' }}</span>
                                                <div class="w-100 d-flex flex-row justify-content-between mt-auto">
                                                    <span class="d-inline-block text-secondary">{{ $type }}</span>
                                                    <span class="d-inline-block text-primary">{{ $lesson->room }}</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endif
                            @else
                                @if(auth()->user()->role_id == '3' || auth()->user()->role_id == '4')
                                    <div class="pair p-2">
                                        <div class="add">
                                            <div class="text-muted">Нет пары</div>
                                        </div>
                                    </div>
                                @else
                                    <div class="pair p-2">
                                        <div class="add">
                                            <a href="{{ route('schedule.add', ['group' => $group, 'pair' => $pair['number'], 'day' => $day]) }}"
                                               class="btn btn-sm btn-outline-secondary mx-auto">Добавить</a>
                                        </div>
                                    </div>
                                @endif
                            @endif
                        @endforeach
                    </div>
                </div>
            @endforeach

        </div>
        @endsection

        @section('scripts')

            <script>
                $('document').ready(function () {
                    $('#prev').click(function () {
                        var week = @json($week);

                        $.ajax({
                            method: 'POST',
                            url: "{{ route('schedule.prevWeek', ['group'=>$group]) }}",
                            data: {week: week}
                        });
                    })
                })
            </script>

@endsection
