@extends('layouts.app')

@section('content')
    <div class="container">
        <h5>{{ $group->name }} {{ $lesson->pair_number }} пара {{ $lesson->date }}</h5>
        <form method="POST">
            @csrf
            <div class="form-group">
                <label for="subject">Выберите предмет</label>
                <select class="custom-select" id="subject" name="subject">
                    <option>Выберите...</option>
                    @foreach($subjects as $subject)
                            <option {{ $lesson->subject_id == $subject->id ? 'selected' : '' }} value="{{ $subject->id }}">{{ $subject->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="teacher">Выберите преподавателя</label>
                <select class="custom-select" id="teacher" name="teacher">
                    <option>Выберите...</option>
                    @foreach($teachers as $teacher)
                        <option {{ $lesson->teacher_id == $teacher->id ? 'selected' : '' }} value="{{ $teacher->id }}">{{ $teacher->second_name }} {{ $teacher->first_name }} {{ $teacher->middle_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="type">Выберите тип пары</label>
                <select class="custom-select" id="type" name="type">
                    <option>Выберите...</option>
                    <option value="lesson" {{ $lesson->type == 'lesson' ? 'selected' : '' }}>Лекция</option>
                    <option value="practice" {{ $lesson->type == 'practice' ? 'selected' : '' }}>Практика</option>
                    <option value="laboratory" {{ $lesson->type == 'laboratory' ? 'selected' : '' }}>Лабораторная работа</option>
                </select>
            </div>
            <div class="form-group">
                <label for="period">Периодичность</label>
                <select class="custom-select" id="period" name="period">
                    <option>Выберите...</option>
                    <option value="1">Каждую неделю</option>
                    <option value="2">По четным неделям</option>
                    <option value="3">По нечетным неделям</option>
                    <option value="4">Определенные недели</option>
                </select>
            </div>
            <div class="form-group" id="periods" style="display: none;">
                <label for="weeks">Недели</label>
                <input type="text" class="form-control" name="weeks" id="weeks" placeholder="Введите недели через запятую">
            </div>
            <div class="form-group">
                <label for="room">Аудитория</label>
                <input type="text" class="form-control" name="room" id="room" placeholder="Введите номер аудитоории..."
                       value="{{ $lesson->room  }}" required>
            </div>

            <button class="btn btn-sm btn-primary" type="submit">Добавить</button>
        </form>
    </div>

@endsection

@section('scripts')
    <script>
        $('document').ready(function () {
            $('#period').change(function () {
                if ($('#period').val() == 4) {
                    $('#periods').slideDown();
                }
                else {
                    $('#periods').slideUp();
                }
            })
        })
    </script>
@endsection

