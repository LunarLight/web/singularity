@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 class="mb-4">Выбор группы</h2>
        <div class="row">
            <div class="col-3">
                <h4>Факультеты</h4>
            </div>
            <div class="col-9">
                <h4>Направления</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-3">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    @foreach($faculties as $key => $faculty)
                        <a class="nav-link {{ $key == 0 ? 'active' : '' }}" id="faculty_tab_{{$faculty->id}}"
                           data-toggle="tab" href="#faculty_content_{{$faculty->id}}" role="tab"
                           aria-controls="home" aria-selected="true">{{$faculty->name}}</a>
                    @endforeach
                </div>
            </div>
            <div class="col-9">
                <div class="tab-content" id="v-pills-tabContent">
                    @foreach($faculties as $key => $faculty)
                        <div class="tab-pane fade {{ $key == 0 ? 'active show' : '' }}" id="faculty_content_{{$faculty->id}}"
                             role="tabpanel">
                            <ul class="nav nav-tabs mb-3" id="directions" role="tablist">
                                @foreach(\App\Direction::where('faculty_id', $faculty->id)->get() as $key => $direction)
                                    <li class="nav-item">
                                        <a class="nav-link {{ $key == 0 ? 'active' : '' }}"
                                           id="direction_tab_{{$direction->id}}" data-toggle="tab"
                                           href="#direction_content_{{$direction->id}}" role="tab"
                                           aria-controls="home" aria-selected="true">{{$direction->name}}</a>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="tab-content" id="directionsContent">
                                @foreach(\App\Direction::where('faculty_id', $faculty->id)->get() as $key => $direction)
                                    @if ($direction->faculty_id == $faculty->id)
                                        <div class="tab-pane fade {{ $key == 0 ? 'active show' : '' }}"
                                             id="direction_content_{{$direction->id}}" role="tabpanel">
                                            <h5>Группы</h5>
                                            @foreach($groups as $group)
                                                @if ($group->direction_id == $direction->id)
                                                    <a href="{{ route('schedule.group', ['group'=>$group]) }}">{{ $group->name }}</a>
                                                @endif
                                            @endforeach
                                        </div>
                                    @endif

                                @endforeach
                            </div>
                        </div>

                    @endforeach
                </div>
            </div>
        </div>
        <div class="tab-content" id="facultiesContent">

        </div>
    </div>



@endsection