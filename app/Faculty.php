<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    protected $guarded = ['id'];

    public function directions() {
        $this->hasMany('App\Direction');
    }
}
