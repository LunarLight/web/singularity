<?php

use App\Group;
use Carbon\Carbon;

function trim_array ($element) {
    if (is_array($element)) {
        return array_map('trim_array', $element);
    }

    return trim($element);
}

function JSONError ($errors = ['0' => 'Произошла ошибка']) {
    $response = [
        'result' => 'error',
        'errors' => []
    ];

    foreach ($errors as $key => $value) {
        if (is_array($value)) {
            foreach ($value as $error) {
                $response['errors'][] = [
                    'code' => $key,
                    'description' => $error
                ];
            }
        } else {
            $response['errors'][] = [
                'code' => $key,
                'description' => $value
            ];
        }
    }

    return $response;
}

function JSONSuccess ($items = null)
{
    $response = [
        'result' => 'success',
        'errors' => []
    ];

    if (!is_null($items)) {
        $response['data'] = $items;
    }

    return $response;
}

function whatSemester(Group $group)
{
    $group_year = mb_substr($group->name, -2);
    $today = Carbon::today();


    $first_sept = Carbon::now();

    $first_sept->year = 2019;
    $first_sept->day = 1;
    $first_sept->month = 9;

    if ($first_sept->greaterThan($today)) {
        $semester = ($today->format('y') - $group_year) * 2;
    } else {
        $semester = ($today->format('y') - $group_year) * 2 - 1;
    }


    return $semester;
}