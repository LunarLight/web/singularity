<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\VarDumper\Cloner\Data;

class Lesson extends Model
{
    public function group() {
       return $this->belongsTo(Group::class);
    }

    public function subject() {
        return $this->belongsTo(Subject::class);
    }

    public function teacher() {
        return $this->belongsTo(Teacher::class);
    }

    public function dates() {
        return $this->hasMany(Date::class);
    }
}
