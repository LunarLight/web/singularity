<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model {
    public function groups () {
        $query = $this->belongsToMany(Group::class, 'groups2subjects2semesters');

        if(isset($this->pivot)) {
            $query = $query->where(function ($q) {
                $q->where('groups2subjects2semesters.subject_id', $this->pivot->subject_id);
            });
        }

        return $query;
    }

    public function subjects () {
        $query = $this->belongsToMany(Subject::class, 'groups2subjects2semesters');

        if(isset($this->pivot)) {
            $query = $query->where(function ($q) {
                $q->where('groups2subjects2semesters.group_id', $this->pivot->group_id);
            });
        }

        return $query;
    }

    public function knowledge () {
        return $this->hasMany(Knowledge::class);
    }
}
