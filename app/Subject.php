<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model {
    use SoftDeletes;

    protected $guarded = ['id'];

    public function teachers () {
        return $this->belongsToMany(Teacher::class, 'subjects2teachers');
    }

    public function directions () {
        return $this->belongsToMany(Direction::class, 'subjects2directions');
    }

    public function groups () {
        $query = $this->belongsToMany(Group::class, 'groups2subjects2semesters');

        if(isset($this->pivot)) {
            $query = $query->where(function ($q) {
                $q->where('groups2subjects2semesters.semester_id', $this->pivot->semester_id);
            });
        }

        return $query;
    }

    public function semesters () {
        $query = $this->belongsToMany(Semester::class, 'groups2subjects2semesters');

        if(isset($this->pivot)) {
            $query = $query->where(function ($q) {
                $q->where('groups2subjects2semesters.group_id', $this->pivot->group_id);
            });
        }

        return $query;
    }

    public function lessons () {
        $this->hasMany(Lesson::class);
    }


    public function knowledge () {
        return $this->hasMany(Knowledge::class);
    }
}
