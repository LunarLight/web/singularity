<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'first_name' => $this->first_name,
            'second_name' => $this->second_name,
            'middle_name' => $this->middle_name,
            'group' => $this->group,
            'cipher' => $this->cipher,
            'faculty' => $this->faculty,
            'direction' => $this->direction,
        ];
    }
}
