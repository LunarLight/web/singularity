<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LessonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $pairs = [
                '1' => '9:00 - 10:30',
                '2' => '10:40 - 12:10',
                '3' => '13:00 - 14:30',
                '4' => '14:40 - 16:10',
                '5' => '16:20 - 17:50',
                '6' => '18:00 - 19:30',
        ];

        $type = [
            'practice' => 'Практика',
            'lesson' => 'Лекция',
            'laboratory' => 'Лабораторные',
        ];

        return [
            'id' => $this['day']->id,

            'title' => $this['lesson']->subject->name,
            'time' => $pairs[$this['lesson']->pair_number],
            'teacher' => $this['lesson']->teacher->second_name . ' ' . mb_substr($this['lesson']->teacher->first_name, 0, 1) . '. ' . mb_substr($this['lesson']->teacher->middle_name, 0, 1) . '.' ,
            'room' => $this['lesson']->room,
            'type' => $type[$this['lesson']->type],
        ];
    }
}
