<?php

namespace App\Http\Controllers;

use App\Knowledge;
use App\Semester;
use App\Subject;
use App\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KnowledgeController extends Controller
{
    public function index()
    {
        $semesters = Semester::all();

        if (Auth::user()->role_id == '4') {
            $teacher = Teacher::where('user_id', Auth::user()->id)->first();
            $subjects = $teacher->subjects()->get();
            $id = $teacher->id;
            $knowledge = Knowledge::where('teacher_id', $id)->get();
        }

        return view('knowledge.index', compact('semesters', 'subjects', 'id', 'knowledge'));
    }


    public function add(Request $request)
    {
        $knowledge = new Knowledge();

        $this->validate($request, [
            'file_name' => 'required',
            'file' => 'required',
        ]);

        $knowledge->semester_id = $request->semester_id;
        $knowledge->subject_id = $request->subject_id;
        $knowledge->teacher_id = $request->teacher_id;
        $knowledge->name = $request->file_name;

        if ($request->hasFile('file')) {
            $name = explode('.', $request->file->getClientOriginalName());
            $extension = end($name);

            $path = $request->file->storeAs('public/files', time() . '.' . $extension);

            $knowledge->path = $path;
        }

        $knowledge->save();

        return back()->with('success', true);
    }
}
