<?php

namespace App\Http\Controllers;

use App\Date;
use App\Direction;
use App\Faculty;
use App\Group;
use App\Lesson;
use App\Semester;
use App\Subject;
use App\Teacher;
use Carbon\Carbon;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Integer;

class LessonController extends Controller
{
    public function index()
    {
        $faculties = Faculty::all();
        $directions = Direction::all();
        $groups = Group::all();

        return view('schedule.index', compact(['faculties', 'directions', 'groups']));
    }
    
    public function group(Group $group, Request $request)
    {
        $today = Carbon::today();
        $week = [$today->startOfWeek()];

        $pairs = [
            [
                'number' => '1',
                'pair_time' => '9:00 - 10:30'
            ],
            [
                'number' => '2',
                'pair_time' => '10:40 - 12:10',
            ],
            [
                'number' => '3',
                'pair_time' => '13:00 - 14:30',
            ],
            [
                'number' => '4',
                'pair_time' => '14:40 - 16:10',
            ],
            [
                'number' => '5',
                'pair_time' => '16:20 - 17:50',
            ],
            [
                'number' => '6',
                'pair_time' => '18:00 - 19:30',
            ],
        ];


        for ($i = 1; $i < 6; $i++) {
            $week[] = $week[$i - 1]->copy()->addDay();
        }

        return view('schedule.group', compact(['group', 'week', 'pairs']));
    }

    public function add(Request $request, Group $group, $pair, $day)
    {

        $date = Carbon::parse($day);


        if ($request->isMethod('post')) {

            $this->validate($request, [
                'type' => 'required',
                'room' => 'required',
                'period' => 'required',
            ]);
            if (!$request->weeks) {
                $weeks = '';
            } else $weeks = $request->weeks;

            $lesson = new Lesson();

            $lesson->group_id = $group->id;
            $lesson->pair_number = $pair;
            $lesson->subject_id = $request->subject;
            $lesson->teacher_id = $request->teacher;
            $lesson->type = $request->type;
            $lesson->day_of_week = $date->dayOfWeek;
            $lesson->room = $request->room;

            $lesson->save();

            $this->addSchedule($lesson, $request->period, $date, $weeks);

            return redirect()->route('schedule.group', ['group' => $group]);
        }


        $semester = $group->semesters->find($this->whatSemester($group));
        $subjects = $semester->subjects->load("teachers");

        $teachers = $subjects->pluck('teachers')->collapse()->unique();

        return view('schedule.add', compact(['group', 'pair', 'date', 'teachers', 'subjects', 'semester']));
    }

    public function update(Request $request, Lesson $lesson, $day)
    {
        $date = Carbon::parse($day);

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'type' => 'required',
                'room' => 'required',
                'period' => 'required',
            ]);

            if (!$request->weeks) {
                $weeks = '';
            } else $weeks = $request->weeks;

            $new_lesson = new Lesson();

            $new_lesson->group_id = $lesson->group_id;
            $new_lesson->pair_number = $lesson->pair_number;
            $new_lesson->subject_id = $request->subject;
            $new_lesson->teacher_id = $request->teacher;
            $new_lesson->type = $request->type;
            $new_lesson->room = $request->room;
            $new_lesson->day_of_week = $lesson->day_of_week;

            Date::where('lesson_id', $lesson->id)->delete();
            $lesson->delete();
            $new_lesson->save();

            $this->addSchedule($new_lesson, $request->period, $date, $weeks);


            return redirect()->route('schedule.group', ['group' => $lesson->group_id]);
        }

        $group = Group::find($lesson->group_id)->first();
        $semester = $group->semesters->find($this->whatSemester($group));
        $subjects = $semester->subjects->load("teachers");

        $teachers = $subjects->pluck('teachers')->collapse()->unique();

        return view('schedule.update', compact(['group', 'lesson', 'teachers', 'subjects']));
    }

    public function whatSemester(Group $group)
    {
        $group_year = mb_substr($group->name, -2);
        $today = Carbon::today();


        $first_sept = Carbon::now();

        $first_sept->year = $today->year;
        $first_sept->day = 1;
        $first_sept->month = 9;

        if ($first_sept->greaterThan($today)) {
            $semester = ($today->format('y') - $group_year) * 2;
        } else {
            $semester = ($today->format('y') - $group_year) * 2 - 1;
        }


        return $semester;
    }

    public function addSchedule(Lesson $lesson, $period, $date, $weeks)
    {
        $first_day = Carbon::now();

        $day = Carbon::parse($date);

        $semester = Semester::find($this->whatSemester(Group::find($lesson->group_id)))->number;

        if (!($semester % 2)) {
            $first_day->day = 1;
            $first_day->month = 2;
            $first_day->day += 7;
            while ($first_day->dayOfWeek != 1) {
                $first_day->day += 1;
            }
            $last_day = $first_day->copy();
            $last_day->day += 112;
        } else {
            $first_day->day = 1;
            $first_day->month = 9;

            if ($first_day->dayOfWeek == 7) {
                $first_day->day = 2;
            }

            $last_day = $first_day->copy();
            $last_day->day += 112;
        }

        $first_day->startOfWeek();


        $days = $day->copy()->dayOfWeek - $day->startOfWeek()->dayOfWeek;


        $first_day->day += $days;

        switch ($period) {
            case 1:
                {
                    while ($last_day->greaterThan($first_day)) {
                        $date = new Date();

                        $date->lesson_id = $lesson->id;
                        $date->date = $first_day->copy();
                        $first_day->addWeek();

                        $date->save();
                    }
                    break;
                }
            case 2:
                {
                    $first_day->addWeek();
                    while ($last_day->greaterThan($first_day)) {
                        $date = new Date();

                        $date->lesson_id = $lesson->id;
                        $date->date = $first_day->copy();
                        $first_day->addWeek(2);

                        $date->save();
                    }
                    break;
                }
            case 3:
                {
                    while ($last_day->greaterThan($first_day)) {
                        $date = new Date();

                        $date->lesson_id = $lesson->id;
                        $date->date = $first_day->copy();
                        $first_day->addWeek(2);

                        $date->save();
                    }
                    break;
                }
            case 4:
                {
                    $weeks_array = explode(',', $weeks);

                    foreach ($weeks_array as $week) {
                        $add_day = $first_day->addWeek($week-1);
                        if ($last_day->greaterThan($add_day))
                            $date = new Date();

                        $date->lesson_id = $lesson->id;
                        $date->date = $first_day->copy();

                        $date->save();
                    }
                }
                break;
        }
    }

}
