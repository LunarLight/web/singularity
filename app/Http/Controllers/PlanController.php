<?php

namespace App\Http\Controllers;

use App\Direction;
use App\Faculty;
use App\Group;
use App\Semester;
use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PlanController extends Controller
{
    public function semesters()
    {
        $semesters = Semester::all();

        return view('plans.semesters', compact(['semesters']));
    }

    public function semester(Semester $semester)
    {
        $faculties = Faculty::all();
        $directions = Direction::all();
        $groups = Group::all();

        //dd($semester->groups->first()->subjects);
        //dd($groups->first()->semesters->first()->subjects);

        return view('plans.semester', compact(['faculties', 'directions', 'groups', 'semester']));
    }

    public function group(Request $request, Semester $semester, Group $group)
    {
        $subjects = Subject::all();

        if ($request->isMethod('post')) {

            $semester_to_subjects = [];

            if ($request->has('subjects')) {
                foreach ($request->subjects as $subject) {
                    $semester_to_subjects[] = ['semester_id' => $semester->id, 'subject_id' => $subject];
                }
            }

            DB::table('groups2subjects2semesters')->where(['group_id' => $group->id, 'semester_id' => $semester->id])->delete();
            $group->semesters()->attach($semester_to_subjects);

            return redirect()->route('plans.semester', ['semester' => $semester]);
        }


        return view('plans.group', compact(['semester', 'group', 'subjects']));
    }
}