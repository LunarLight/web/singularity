<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\StudentResource;
use App\Http\Resources\TeacherResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class AuthController extends Controller {
    public function login (Request $request) {
        $validator = validator($request->all(), [
            'login' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(JSONError($validator->errors()->toArray()));
        }

        if ($token = auth()->guard('api')->attempt($request->only(['login', 'password']))) {
            $user = auth()->guard('api')->user();

            $role = $user->role->name;

            if($user->hasRole('student')) {
                $user = StudentResource::make($user->student);
            } else {
                $user = TeacherResource::make($user->teacher);
            }

            $user = collect($user->toArray([]))->merge(['token' => $token, 'role' => $role]);

            return response()->json(JSONSuccess($user));
        }

        return response()->json(JSONError(['login' => __('auth.failed')]));
    }

    public function refresh () {
        try {
            if ($token = auth()->guard('api')->refresh()) {
                return response()->json(JSONSuccess(['token' => $token]));
            }

            return response()->json(JSONError(['unauthenticated' => 'Необходимо авторизоваться.']));
        } catch (TokenInvalidException $e) {
            return response()->json(JSONError(['invalid' => 'Невалидный токен.']));
        } catch (JWTException $e) {
            return response()->json(JSONError(['unauthenticated' => 'Необходимо авторизоваться.']));
        }
    }

    public function logout () {
        auth()->guard('api')->logout();

        return response()->json(JSONSuccess());
    }

    public function user () {
        return response()->json(JSONSuccess(auth()->user()));
    }
}
