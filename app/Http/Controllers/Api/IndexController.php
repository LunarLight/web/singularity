<?php

namespace App\Http\Controllers\Api;

use App\Attendance;
use App\Date;
use App\Group;
use App\Http\Resources\AttendanceResource;
use App\Http\Resources\LessonResource;
use App\Http\Resources\PersonResource;
use App\Lesson;
use App\Semester;
use App\Student;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller {
    public function students (Request $request) {
        $students = Student::where('group', auth()->user()->student->group)->get();

        return response()->json(JSONSuccess(PersonResource::collection($students)));
    }

    public function studentsByLesson (Request $request, $id) {
        $lesson = Lesson::find($id);

        $students = Student::where('group', $lesson->group()->name)->get();

        return response()->json(JSONSuccess(PersonResource::collection($students)));

    }

    public $days = [];

    public function schedule () {
        //$semester = whatSemester(auth()->user()->group);

        $semester = 8;
        //$group = Group::where('name', auth()->user()->group)->first()->id;

        $group = 1;
        $arr = [];

        $first_day = Carbon::now();

        $week = ['type' => false, 'days' => []];

        if (!($semester % 2)) {
            $first_day->day = 1;
            $first_day->month = 2;
            $first_day->day += 7;
            while ($first_day->dayOfWeek != 1) {
                $first_day->day += 1;
            }
            $last_day = $first_day->copy();
            $last_day->day += 112;
        } else {
            $first_day->day = 1;
            $first_day->month = 9;

            if ($first_day->dayOfWeek == 7) {
                $first_day->day = 2;
            }

            $last_day = $first_day->copy();
            $last_day->day += 112;
        }

        $first_day->startOfWeek();

        $number = $first_day->weekOfYear;

        $lessons = Lesson::whereHas('dates', function ($query) use ($first_day, $last_day) {
            $query->whereBetween('date', [$first_day->format('Y-m-d'), $last_day->format('Y-m-d')]);
        })->where('group_id', $group)->with('dates')->get();


        while (!$first_day->greaterThan($last_day)) {
            if ($first_day->dayOfWeek != 0) {
                if ($number != $first_day->weekOfYear) {
                    $arr[] = $week;
                    $week = ['type' => !$week['type'], 'days' => []];
                    $number = $first_day->weekOfYear;
                }

                $lessons_today = $lessons->filter(function ($lesson) use ($first_day) {
                    $day = $lesson->dates->filter(function ($day) use ($first_day) {
                        return $day->date == $first_day;
                    })->first();

                    $day_id = $day ? $day->id : null;
                    $lesson->day_id = $day_id;

                    return (bool)$day;
                })->map(function ($lesson) use ($first_day) {
                    $day = $lesson->dates->filter(function ($day) use ($first_day) {
                        return $day->date == $first_day;
                    })->first();

                    return ['day' => $day, 'lesson' => $lesson];
                });

                $week['days'][] = [
                    'day' => $first_day->format('Y-m-d'),
                    'lessons' => LessonResource::collection($lessons_today)
                ];
            }

            $first_day->addDay();
        }

        return response()->json(JSONSuccess($arr));
    }

    public function attendances (Request $request, Date $date) {
        if ($request->isMethod('post')) {
            $checklist = json_decode($request->checklist);

            foreach ($checklist as $attendance) {
                Attendance::updateOrCreate(['date_id' => $attendance->date_id, 'student_id' => $attendance->person_id], ['checked' => $attendance->checked]);
            }

            return response()->json(JSONSuccess());
        }

        $attendance = AttendanceGResource::collection(Attendance::where('date_id', $date->id)->get());

        return response()->json(JSONSuccess($attendance));
    }
}
