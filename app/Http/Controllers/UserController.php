<?php

namespace App\Http\Controllers;

use App\Direction;
use App\Faculty;
use App\Group;
use App\Role;
use App\Student;
use App\Teacher;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function create(Request $request)
    {
        $roles = Role::all();
        $groups = Group::all();
        $faculties = Faculty::all();
        $directions = Direction::all();


        if ($request->isMethod('post')) {

            $user = new User();

            $this->validate($request, [
                'login' => 'required|unique:users',
                'password' => 'required|min:6',
            ]);

            $user->login = $request->login;
            $user->password = bcrypt($request->password);
            $user->role_id = $request->role;

            $user->save();

            if ($user->hasRole('student')) {
                $this->addStudent($request, $user);
            }

            if ($user->hasRole('teacher')) {
                $this->addTeacher($request, $user);
            }


            return redirect()->route('index');
        }

        return view('users.create', compact(['roles']));
    }

    public function addStudent($request, $user)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'second_name' => 'required',
            'cipher' => 'required|unique:students',
            'group' => 'required',
            'direction' => 'required',
            'faculty' => 'required',
        ]);

        $faculty = Faculty::firstOrCreate(['name' => mb_strtoupper($request->faculty)]);

        $direction = Direction::firstOrCreate(['name' => mb_strtoupper($request->direction), 'faculty_id' => $faculty->id]);


        $group = Group::firstOrCreate(['name' => mb_strtoupper($request->group), 'direction_id' => $direction->id]);

        $student = new Student();

        $student->first_name = $request->first_name;
        $student->second_name = $request->second_name;
        $student->middle_name = $request->middle_name;
        $student->user_id = $user->id;
        $student->cipher = mb_strtoupper($request->cipher);
        $student->group = mb_strtoupper($request->group);
        $student->faculty = mb_strtoupper($request->faculty);
        $student->direction = mb_strtoupper($request->direction);

        $student->save();
    }

    public function addTeacher($request, $user)
    {
        $teacher = new Teacher();

        $this->validate($request, [
            'first_name' => 'required',
            'second_name' => 'required',
        ]);

        $teacher->first_name = $request->first_name;
        $teacher->second_name = $request->second_name;
        $teacher->middle_name = $request->middle_name;
        $teacher->user_id = $user->id;

        $teacher->save();
    }


}
