<?php

namespace App\Http\Controllers;

use App\Direction;
use App\Lesson;
use App\Subject;
use App\Teacher;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Integer;

class SubjectController extends Controller
{
    public function view()
    {
        $subjects = Subject::all();

        return view('subjects.index', compact(['subjects']));
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {

            $subject = new Subject();

            $this->validate($request, [
                'name' => 'required',
            ]);

            $subject->name = $request->name;

            $subject->save();

            return redirect()->route('subjects.index');
        }

        return view('subjects.create');
    }

    public function update(Request $request, Subject $subject)
    {
        $teachers = Teacher::all();
        $directions = Direction::all();


        if ($request->isMethod('post')) {

            $this->validate($request, [
                'name' => 'required',
            ]);

            $subject->name = $request->name;

            $subject->save();

            $subject->teachers()->sync($request->teachers);

            $subject->directions()->sync($request->directions);

            return redirect()->route('subjects.index', compact(['teachers', 'directions']));
        }


        return view('subjects.update', compact(['subject', 'teachers', 'directions']));
    }

    public function delete(Subject $subject)
    {
        $subject->delete();


//        where(['id' => $subject->id])->delete();

        return redirect()->route('subjects.index');
    }
}
