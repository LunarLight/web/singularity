<?php

namespace App\Http\Controllers;

use App\Faculty;
use App\Group;
use App\Direction;
use App\Student;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function view() {
        $faculties = Faculty::all();

        return view('groups.index', compact(['faculties']));
    }

    public function directions($id) {
        $faculty = Faculty::find($id);

        $directions = Direction::where('faculty_id', $faculty->id)->get();

        return view('groups.direction', compact(['directions']));
    }

    public function groups($id) {
        $direction = Direction::find($id);

        $groups = Group::where('direction_id', $direction->id)->get();

        return view('groups.group', compact(['groups']));
    }


    public function students($id) {
        $group = Group::find($id);

        $students = Student::where('group', $group->name)->get();

        return view('groups.student', compact(['group', 'students']));
    }
}
