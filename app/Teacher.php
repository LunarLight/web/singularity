<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    public function user() {
        return $this->belongsTo(User::class);
    }

    public function lessons () {
        $this->hasMany(Lesson::class);
    }

    public function subjects () {
        return $this->belongsToMany(Subject::class, 'subjects2teachers');
    }

    public function knowledge () {
        return $this->hasMany(Knowledge::class);
    }
}
