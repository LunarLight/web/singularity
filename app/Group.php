<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model {
    protected $guarded = ['id'];

    public function direction () {
        $this->belongsTo('App\Direction');
    }

    public function subjects () {
        $query = $this->belongsToMany(Subject::class, 'groups2subjects2semesters');

        if(isset($this->pivot)) {
            $query = $query->where(function ($q) {
                $q->where('groups2subjects2semesters.semester_id', $this->pivot->semester_id);
            });
        }

        return $query;
    }

    public function semesters () {
        $query = $this->belongsToMany(Semester::class, 'groups2subjects2semesters');

        if(isset($this->pivot)) {
            $query = $query->where(function ($q) {
                $q->where('groups2subjects2semesters.subject_id', $this->pivot->subject_id);
            });
        }

        return $query;
    }

    public function lessons () {
        $this->hasMany(Lesson::class);
    }
}
