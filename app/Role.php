<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users() {
        $this->hasMany(Role::class);
    }

    public function has ($name) {
        return $this->name === $name;
    }

}
