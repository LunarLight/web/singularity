<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Knowledge extends Model
{
    public function teacher () {
        return $this->belongsTo(Teacher::class);
    }

    public function subject () {
        return $this->belongsTo(Subject::class);
    }

    public function semester () {
        return $this->belongsTo(Semester::class);
    }
}
