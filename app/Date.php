<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Date extends Model
{
    public $dates = ['date'];

    public function lesson() {
       return $this->belongsTo(Lesson::class);
    }

}
