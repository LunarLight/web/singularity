<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject {
    use Notifiable, SoftDeletes;

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function student() {
        return $this->hasOne(Student::class);
    }

    public function teacher() {
         return $this->hasOne(Teacher::class);
    }

    public function role() {
        return $this->belongsTo(Role::class);
    }

    public function isSuperAdmin () {
        return $this->role->has('super_admin');
    }

    public function hasRole ($name) {
        if (is_array($name)) {
            $has = false;

            foreach ($name as $val) {
                if ($this->role->has($name)) {
                    $has = true;
                }
            }

            return $has;
        }

        return $this->role->has($name);
    }
}
