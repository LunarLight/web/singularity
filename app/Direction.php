<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Direction extends Model
{
    protected $guarded = ['id'];


    public function groups() {
        $this->hasMany(App\Group::class);
    }

    public function faculty() {
        $this->belongsTo(App\Faculty::class);
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class, 'subjects2directions');
    }
}
