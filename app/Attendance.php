<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model {
    protected $guarded = ['id'];

    public function student () {
        return $this->belongsTo(Student::class);
    }

    public function lesson () {
        return $this->belongsTo(Lesson::class);
    }
}
